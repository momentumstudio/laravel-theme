<?php

namespace MomentumStudio\LaravelTheme\Test;

use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Filesystem\FilesystemManager;
use League\Flysystem\Adapter\Local;
use MomentumStudio\LaravelTheme\Facades\Theme as ThemeFacade;
use MomentumStudio\LaravelTheme\LaravelThemeServiceProvider;
use MomentumStudio\LaravelTheme\Theme;
use Orchestra\Testbench\TestCase;

/**
 * @coversDefaultClass \MomentumStudio\LaravelTheme\Theme
 * @covers \MomentumStudio\LaravelTheme\LaravelThemeServiceProvider
 * @covers ::__construct
 * @covers ::<!public>
 */
class ThemeTest extends TestCase
{
    /** @var Filesystem */
    private $files;

    /**
     * @covers ::set
     */
    public function testDefault(): void
    {
        $expected = 'Hello World.';
        $this->files->put('default/test.blade.php', $expected);

        $actual = trim(view('test')->render(), "\n");

        self::assertEquals($expected, $actual);
    }

    /**
     * @covers ::set
     */
    public function testSet(): void
    {
        $expected = 'Hello World.';
        $this->files->put('test/test.blade.php', $expected);

        ThemeFacade::set('test');
        $actual = trim(view('test')->render(), "\n");

        self::assertEquals($expected, $actual);
    }

    /**
     * @covers ::set
     * @covers ::get
     */
    public function testGet(): void
    {
        $slug = 'test';
        ThemeFacade::set($slug);

        self::assertEquals($slug, ThemeFacade::get());
    }

    /** @inheritdoc */
    protected function getPackageProviders($app): array
    {
        return [
            LaravelThemeServiceProvider::class
        ];
    }

    /** @inheritdoc */
    protected function getPackageAliases($app): array
    {
        return [
            'Theme' => ThemeFacade::class
        ];
    }

    /** @inheritdoc */
    protected function getEnvironmentSetUp($app): void
    {
        config()->set('theme.default', 'default');
        $root = __DIR__.'/files';

        /** @var FilesystemManager $filesystemManager */
        $filesystemManager = $app->make('filesystem');

        if (defined('League\Flysystem\Adapter\Local::DISALLOW_LINKS')) {
            $this->files = $filesystemManager->createLocalDriver([
                'root' => $root
            ]);
        } else {
            // Laravel 5.5 is missing a constant in lower versions
            // Causing `createLocalDriver` to fail
            // So here we create the adapter manually
            $this->files = new FilesystemAdapter(new \League\Flysystem\Filesystem(
                new Local($root)
            ));
        }

        $app->make('config')->set('theme.path', $root);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        foreach ($this->files->allDirectories() as $directory) {
            $this->files->deleteDirectory($directory);
        }
    }
}
