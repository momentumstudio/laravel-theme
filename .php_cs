<?php

return (new MomentumStudio\CodingStandards\LibraryConfigBuilder)
    ->project('Laravel Theme')
    ->openSourceCopyright('MIT', 'https://opensource.org/licenses/MIT', 'https://gitlab.com/momentumstudio/laravel-theme')
    ->finder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__ . '/src')
    )
    ->requirePhp('7.2.0')
    ->build()
    ;
