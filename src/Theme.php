<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Laravel Theme (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/laravel-theme
 */

declare(strict_types=1);

namespace MomentumStudio\LaravelTheme;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;

class Theme
{
    /** @var Application */
    protected $app;

    /** @var string|null */
    protected $current = null;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get current theme.
     */
    public function get(): ?string
    {
        return $this->current;
    }

    /**
     * Set current theme.
     *
     * @throws BindingResolutionException
     */
    public function set(string $themeSlug): void
    {
        /** @var Repository $config */
        $config = $this->app->make('config');
        $paths = $this->app->make('config')->get('view.paths');
        array_unshift(
            $paths,
            $config->get('theme.path') . '/' . $themeSlug
        );

        /** @var Factory $view */
        $view = $this->app->make('view');
        $oldViewFinder = $view->getFinder();
        assert($oldViewFinder instanceof FileViewFinder);
        $viewFinder = new FileViewFinder($this->app->make('files'), $paths);

        foreach ($oldViewFinder->getHints() as $namespace => $namespacePaths) {
            $viewFinder->addNamespace($namespace, $namespacePaths);
        }

        $view->setFinder($viewFinder);
        $this->app->bind('view', function () use ($view): Factory {
            return $view;
        });

        $this->current = $themeSlug;
    }
}
