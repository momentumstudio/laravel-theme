image: registry.gitlab.com/momentumstudio/ci-build:7.2

stages:
  - build
  - lint
  - test
  - pages

variables:
  COMPOSER_ALLOW_SUPERUSER: 1

composer:
  stage: build
  cache:
    key: ${CI_COMMIT_REF_SLUG}-composer
    paths:
      - vendor/
  script:
    - composer update --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
  artifacts:
    paths:
      - composer.lock
      - vendor/

security:
  stage: lint
  dependencies:
    - composer
  script:
    - wget https://get.sensiolabs.org/security-checker.phar
    - php security-checker.phar security:check

phpstan:
  stage: lint
  dependencies:
    - composer
  script:
    - vendor/bin/phpstan analyse --no-progress

codestyle:
  stage: lint
  dependencies:
    - composer
  script:
    - composer cstest

composer_require:
  stage: lint
  dependencies:
    - composer
  script:
    - wget https://github.com/maglnet/ComposerRequireChecker/releases/download/2.0.0/composer-require-checker.phar
    - php composer-require-checker.phar

coverage:
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  stage: test
  dependencies:
    - composer
  script:
    - vendor/bin/phpunit -d memory_limit=512M --coverage-text --coverage-html coverage --colors=never
  artifacts:
    paths:
      - coverage/
    expire_in: 30 days

.test:
  stage: test
  script:
    - composer require laravel/framework:"$LARAVEL_VERSION" --prefer-dist --no-interaction --no-progress --no-update
    - composer require --dev orchestra/testbench:"$TESTBENCH_VERSION" --prefer-dist --no-interaction --no-progress --no-update
    - composer update --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - vendor/bin/phpunit -d memory_limit=512M --colors=never

laravel-5.5:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '5.5.*'
    TESTBENCH_VERSION: '3.5.*'

laravel-5.5:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '5.5.*'
    TESTBENCH_VERSION: '3.5.*'

laravel-5.5:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '5.5.*'
    TESTBENCH_VERSION: '3.5.*'

laravel-5.6:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '5.6.*'
    TESTBENCH_VERSION: '3.6.*'

laravel-5.6:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '5.6.*'
    TESTBENCH_VERSION: '3.6.*'

laravel-5.6:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '5.6.*'
    TESTBENCH_VERSION: '3.6.*'

laravel-5.7:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '5.7.*'
    TESTBENCH_VERSION: '3.7.*'

laravel-5.7:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '5.7.*'
    TESTBENCH_VERSION: '3.7.*'

laravel-5.7:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '5.7.*'
    TESTBENCH_VERSION: '3.7.*'

laravel-5.8:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '5.8.*'
    TESTBENCH_VERSION: '3.8.*'

laravel-5.8:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '5.8.*'
    TESTBENCH_VERSION: '3.8.*'

laravel-5.8:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '5.8.*'
    TESTBENCH_VERSION: '3.8.*'

laravel-6:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '^6'
    TESTBENCH_VERSION: '^4'

laravel-6:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '^6'
    TESTBENCH_VERSION: '^4'

laravel-6:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '^6'
    TESTBENCH_VERSION: '^4'

laravel-7:php-7.2:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.2
  variables:
    LARAVEL_VERSION: '^7'
    TESTBENCH_VERSION: '^5'

laravel-7:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '^7'
    TESTBENCH_VERSION: '^5'

laravel-7:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '^7'
    TESTBENCH_VERSION: '^5'

laravel-8:php-7.3:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.3
  variables:
    LARAVEL_VERSION: '^8'
    TESTBENCH_VERSION: '^6'

laravel-8:php-7.4:
  extends: .test
  image: registry.gitlab.com/momentumstudio/ci-build:7.4
  variables:
    LARAVEL_VERSION: '^8'
    TESTBENCH_VERSION: '^6'

pages:
  stage: pages
  image: alpine:latest
  dependencies:
    - coverage
  script:
    - mv coverage/ public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master
