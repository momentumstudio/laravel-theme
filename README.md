<p align="center">
    <img src="https://gitlab.com/momentumstudio/laravel-theme/raw/master/logo.png" alt="Logo Image">
</p>

# Laravel Theme

This project is licensed [MIT](https://opensource.org/licenses/MIT).

Laravel Theme by [Momentum Studio](https://momentum.studio) is a package that allows the developer to create multiple sets
of templates that can be switched at runtime. Allowing for users to set their own
theme, or simply for trying out a new theme to a select few users.

* Adds your theme path to Laravel's default `ViewFinder`, making sure it can
support other theme-related packages.
* Uses a simple Facade to set your current theme.
* Config overridable via environment variables or your own config file.

## Installation & Usage

> Requires

* PHP 7.2+
* [Laravel](https://laravel.com) 5.5+

Use Composer to install Laravel Theme as a dependency into your Laravel project:

`composer require momentumstudio/laravel-theme`

The package will automatically register itself. You can publish the config file with:

`php artisan vendor:publish --provider="MomentumStudio\LaravelTheme\LaravelThemeServiceProvider"`

Alternatively, you can just set the environment variables in your `.env` file:

```
DEFAULT_THEME=default
THEMES_PATH=themes
```

This is the contents of the default configuration file:

```php
return [

    /**
     * Set default theme to be chosen on startup
     * Set to `null` to use default views (resources/views)
     */
    'default' => env('DEFAULT_THEME', null),

    /**
     * Set the base directory to find themes
     */
    'path' => env('THEMES_PATH', base_path('themes')),
];
```

You can set `default` to `null` to keep using `resources/views` until you set otherwise at runtime.
_Good for importing this package into existing projects._

Then, to set a theme during runtime, call the provided Facade's `set` method:

```php
\MomentumStudio\LaravelTheme\Facades\Theme::set('directory-name');
```

This will then search your themes path for templates matching the name when calling
Laravel's `view()` (or equivalent).

For example, setting my theme to `mytheme` and calling `view('my-view')` will search
`themes/mytheme/my-view.blade.php` first before reverting to `resources/views/my-view.blade.php`.

# Contributing

Please see [CONTRIBUTING.md](https://gitlab.com/momentumstudio/laravel-theme/blob/master/CONTRIBUTING.md) for details
about how you can help!