<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Laravel Theme (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/laravel-theme
 */

declare(strict_types=1);

namespace MomentumStudio\LaravelTheme\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @codeCoverageIgnore
 *
 * @method static void set(string $slug)
 */
class Theme extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'laravel-theme';
    }
}
