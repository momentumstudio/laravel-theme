<?php

/**
 * Laravel Theme by Momentum Studio
 * https://gitlab.com/momentumstudio/laravel-theme
 */

return [

    /**
     * Set default theme to be chosen on startup
     * Set to `null` to use default views (resources/views)
     */
    'default' => env('DEFAULT_THEME', null),

    /**
     * Set the base directory to find themes
     */
    'path' => env('THEMES_PATH', base_path('themes')),
];