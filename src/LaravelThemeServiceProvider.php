<?php

/**
 * (c) Copyright Momentum Studio Ltd. All Rights Reserved.
 * This code is a part of Laravel Theme (an open source project) under the MIT license.
 * You must adhere to the licensing restrictions found at https://opensource.org/licenses/MIT
 * For support, please visit https://gitlab.com/momentumstudio/laravel-theme
 */

declare(strict_types=1);

namespace MomentumStudio\LaravelTheme;

use Illuminate\Support\ServiceProvider;
use MomentumStudio\LaravelTheme\Facades\Theme as ThemeFacade;

class LaravelThemeServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/theme.php' => config_path('theme.php'),
        ], 'config');

        $this->app->singleton('laravel-theme', Theme::class);

        if (config('theme.default', null) !== null) {
            ThemeFacade::set(config('theme.default'));
        }
    }

    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/theme.php',
            'theme'
        );
    }

    /**
     * @codeCoverageIgnore
     *
     * @return array<string>
     */
    public function provides(): array
    {
        return ['laravel-theme'];
    }
}
